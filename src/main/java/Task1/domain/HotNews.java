package Task1.domain;


import java.util.ArrayList;

public class HotNews extends News {
    String HotNews;

    public HotNews(ArrayList<String> news, String currnews, String HotNews) {
        super(news, currnews);
        setHotNews(HotNews);
    }

    public String getHotNews() {
        return HotNews;
    }

    public void setHotNews(String HotNews) {
        HotNews = HotNews;
    }

    @Override
    public String toString() {
        return " HOT NEWS:" + " " + HotNews;
    }
}
