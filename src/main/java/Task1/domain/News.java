package Task1.domain;

import java.util.ArrayList;

//superclass
public class News{
    ArrayList <String> news;

    public News (ArrayList<String> news, String currnews){
        addNews(currnews);
    }

    public ArrayList<String> getNews() {
        return news;
    }

    public void setNews(ArrayList<String> news) {
        setNews(news);
    }

    public void addNews(String currnews){
        news.add(currnews);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
