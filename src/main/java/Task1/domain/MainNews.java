package Task1.domain;

import java.util.ArrayList;

public class MainNews extends News{
    String mainNews;

    public MainNews(ArrayList<String> news, String currnews, String mainNews) {
        super(news, currnews);
        setMainNews(mainNews);
    }

    public String getMainNews() {
        return mainNews;
    }

    public void setMainNews(String mainNews) {
        this.mainNews = mainNews;
    }
}
